<!-- Name this issue "Week 8 - Explore others' paths" -->
Spend this week practicing the AMPP and ABC skills to explore others' paths.

- Ask (what do you think about ...?)
- Mirror (you seem x, what's up/is this ok/are you sure?)
- Paraphrase (so you're saying...?)
- Prime (Do you think that...?)

-----

- Agree, Build, Compare

-----

- [ ] Use AMPP skills in 5 conversations, attempt to use each one at least once
- [ ] Use ABC in 3 conversations that are brought to you where you might normally be defensive.
- [ ] At the end of the week, share what you've noticed about these conversations.

/label ~"status::todo"
/label ~"Crucial Conversations"