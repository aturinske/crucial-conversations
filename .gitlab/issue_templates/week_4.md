<!-- Name this issue "Week 4 - State my path" -->
This week, prepare for conversations by thinking about your path and start stating your path in the conversation.

1. Share your facts
1. Tell your story
1. Ask for others' paths

-----

- [ ] Prepare for 5 conversations by coming up with a way to state your path.
- [ ] State your path in 5 conversations.
- [ ] At the end of the week, share what you've noticed about these conversations.

/label ~"status::todo"
/label ~"Crucial Conversations"