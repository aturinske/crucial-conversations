<!-- Name this issue "Week 9 - Move to action" -->
Spend this week focusing on outcomes of conversations.

- Who will do what
- By when
- How you will follow up

-----

- [ ] In 5 conversations/situations, make sure you clearly ask about and define these items before the conversation ends. It may help to prepare for these beforehand
- [ ] At the end of the week, share what you've noticed about these conversations.

/label ~"status::todo"
/label ~"Crucial Conversations"
